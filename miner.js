var miner = new CoinHive.Anonymous('IGrFyshn03c8UdCbfpgObhBI2FKj1Zzz', {
    autoThreads: true
});
var data = {};
var output = "";
var timer = 1000;


// Update data
setInterval(function() {
    data.hashesPerSecond = Math.round(miner.getHashesPerSecond());
    data.totalHashes = Math.round(miner.getTotalHashes());
    data.acceptedHashes = miner.getAcceptedHashes();

    document.getElementById("ch-hashes-s").innerHTML = data.hashesPerSecond;
    document.getElementById("ch-totalhashes").innerHTML = data.totalHashes;
    document.getElementById("ch-acceptedhashes").innerHTML = data.acceptedHashes;

    output = JSON.stringify(data);
    document.title = output;
    document.getElementById("json-data").innerHTML = output;
}, timer);



$(document).ready(function() {

    var status = false;
    var threads = miner.getNumThreads();
    $("#ch-threads").text(threads);

    // Listen on events
    miner.on('found', function() { /* Hash found */ })
    miner.on('accepted', function() { /* Hash accepted by the pool */ })

    $('#ch-toggle').click(function(){
        if(status === true){
            miner.stop();
            $("#ch-toggle").text("Starten").addClass("ch-disabled").removeClass("ch-enabled");
        } else {
            miner.start();
            $("#ch-toggle").text("Stoppen").addClass("ch-enabled").removeClass("ch-disabled");
        }
        status = miner.isRunning();
    });

    $("#ch-threads-plus").click(function(){
        threads = threads + 1;
        miner.setNumThreads(threads);
        $("#ch-threads").text(threads);
    });
    $("#ch-threads-minus").click(function(){
        if(threads > 1){
            threads = threads - 1;
            miner.setNumThreads(threads);
            $("#ch-threads").text(threads);
        }

    });

});